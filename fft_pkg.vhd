library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.MATH_REAL.ALL;

package fft_pkg is

constant n = 15; -- rozmiar wykorzystanych w obliczeniach liczb
constant m = 7;  -- rozmiar FFT

type complex is 
    record
        r : signed(n downto 0);
        i : signed(n downto 0);
    end record;

type comp_array is array (0 to m) of complex; -- tablic
type comp_array2 is array (0 to 3) of complex; -- tablica w

function add (n1,n2 : complex) return complex;
function sub (n1,n2 : complex) return complex;
function mult (n1,n2 : complex) return complex;

end fft_pkg; 

package body fft_pkg is 

--addition of complex numbers
function add (n1,n2 : complex) return complex is

variable sum : complex;

begin 
sum.r:= signed(n1.r + n2.r, n downto 0);
sum.i:= signed(n1.i + n2.i, n downto 0);
return sum;
end add;

--subtraction of complex numbers.
function sub (n1,n2 : complex) return complex is

variable diff : complex;

begin 
diff.r:= signed(n1.r - n2.r, n downto 0);
diff.i:= signed(n1.i - n2.i, n downto 0);
return diff;
end sub;

--multiplication of complex numbers.
function mult (n1,n2 : complex) return complex is

variable prod : complex;

begin 
prod.r:= signed((n1.r * n2.r) - (n1.i * n2.i), n downto 0);
prod.i:= signed((n1.r * n2.i) + (n1.i * n2.r), n downto 0);
return prod;
end mult;

end fft_pkg;