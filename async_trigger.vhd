library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
LIBRARY lpm;
USE lpm.all;
--- wyzwalacz asynchroniczny napisany na podstawie artykułu zamieszczone w dokumentacji
--- składa się się z 3 elementów LFSR, zmodyfikowanego licznika greya, i multipleksera
--- licznik greya gray_counter2 z układem kombinacyjnym zwraca sygnały o różnym stopniu
--- wypełnienia które są podłączone
--- do multipleksera. LFSR wybiera pseudolosowo sygnał z multipleksera
entity async_trigger is
    port (
        result   :out std_logic; -- Output of the counter
         clk    :in  std_logic;-- Input clock
			clk_max :in std_logic;
			rst :in std_logic
     );
 end entity;
 
architecture rtl of async_trigger is
signal sig3: STD_LOGIC_VECTOR(7 downto 0):="00000000";
signal sig2: STD_LOGIC:='0';
signal sig1: STD_LOGIC_VECTOR(7 downto 0):="00000000";
signal sel: STD_LOGIC_VECTOR(7 downto 0):="00000000";

component lfsr
 	generic(constant N: integer := 8);
 	port (
	 	clk			:in  std_logic;		
		reset			:in  std_logic;	
		lfsr_out		:out std_logic_vector (N-1 downto 0)
  	);
end component;


component mux0 IS
	PORT
	(
		data0		: IN STD_LOGIC ;
		data1		: IN STD_LOGIC ;
		data2		: IN STD_LOGIC ;
		data3		: IN STD_LOGIC ;
		data4		: IN STD_LOGIC ;
		data5		: IN STD_LOGIC ;
		data6		: IN STD_LOGIC ;
		data7		: IN STD_LOGIC ;
		sel		: IN STD_LOGIC_VECTOR (2 DOWNTO 0);
		result		: OUT STD_LOGIC 
	);
END component;

component delay_line is
    Generic (
        delay_stages : integer := 8);
    Port ( 
        clk    : in  STD_LOGIC;
		  rst	: in STD_LOGIC;
        din  : in  STD_LOGIC;
        dout    : out  std_logic_vector(1 to delay_stages)
		  );
end component;

begin

delay_line1: delay_line PORT MAP(clk_max,'0',clk,sig1);

lfsr1: lfsr PORT MAP(sig1(7),'0',sel);
sig3(0)<=sig1(0) and sig1(3);
sig3(1)<=sig1(1) and sig1(4) ;
sig3(2)<=sig1(2) and sig1(5);
sig3(3)<=sig1(3) and sig1(6);
sig3(4)<=sig1(4) and sig1(7);
sig3(5)<=sig1(5) and sig1(7);
sig3(6)<=sig1(6) and sig1(7);
sig3(7)<=sig1(7);


--sig3(0)<= sig1(0);
--sig3(1)<=sig1(1);
--sig3(2)<=sig1(2);
--sig3(3)<=sig1(3);
--sig3(4)<=sig1(4);
--sig3(5)<=sig1(5);
--sig3(6)<=sig1(6);
--sig3(7)<=sig1(7);

mux1: mux0 PORT MAP(
sig3(0),
sig3(1),
sig3(2),
sig3(3),
sig3(4),
sig3(5),
sig3(6),
sig3(7),
sel(2 downto 0),
sig2);

latch: process(clk_max)
begin
		if(rising_edge(clk_max)) then
			result<=sig2;
		end if;
end process;
end architecture;